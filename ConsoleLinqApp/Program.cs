﻿using ConsoleLinqApp.Models;
using System;
using System.Linq;

namespace ConsoleLinqApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new BookStoreDBContext())
            {
                //var student = ctx.Products
                //                .Where(s => s.ProductName == "Dell")
                //                .FirstOrDefault<Product>();

                var product = ctx.Products.Find(1);
                //Console.WriteLine($"ProductId: {product?.ProductId}, ProductName: {product?.ProductName}");

                //FirstOrDefault method using Method Syntax
                var product1 = ctx.Products
                    .FirstOrDefault(s => s.ProductName == "Dell");
                //Console.WriteLine($"ProductId: {product1?.ProductId}, ProductName: {product1?.ProductName}");
                //First method using Method Syntax
                var product2 = ctx.Products
                   .First(s => s.ProductName == "Dell");
                //Console.WriteLine($"ProductId: {product2?.ProductId}, ProductName: {product2?.ProductName}");


                //Order By using Query syntax
                var studentsQS = from s in ctx.Products
                                 orderby s.ProductName ascending
                                 select s;

                //Order By using Method syntax
                var studentsMS = ctx.Products.OrderBy(s => s.ProductName).ToList();

                foreach (var student in studentsMS)
                {
                    Console.WriteLine($"ProductId: {student?.ProductId}, ProductName: {student?.ProductName}");
                }

                //Method Syntax
                var selectMethod = ctx.Products.
                                              Select(std => new
                                              {
                                                  ProductId = std.ProductId,
                                                  ProductName = std.ProductName
                                              }).ToList();
                foreach (var student in selectMethod)
                {
                    Console.WriteLine($"ProductId: {student?.ProductId}, ProductName: {student?.ProductName}");
                }
                Console.Read();

            }
        }
    }
}
